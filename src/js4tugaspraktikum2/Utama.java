/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package js4tugaspraktikum2;
import java.util.Scanner;
/**
 *
 * @author ROG SERIE
 */
public class Utama {
    public static void main (String [] args)
    {
        double satu,dua;
        
        Scanner sc1 = new Scanner(System.in);
        Scanner sc2 = new Scanner(System.in);
        
        System.out.print("Masukkan bilangan pertama : ");
        satu = sc1.nextDouble();
        System.out.print("Masukkan bilangan kedua : ");
        dua = sc2.nextDouble();
        
        System.out.println();
        
        Operasi op = new Kalkulator(satu,dua);
        System.out.println("Hasil Penjumlahan = "+ op.Penjumlahan());
      
        System.out.println("Hasil Pengurangan = "+ op.Pengurangan());
        
        System.out.println("Hasil Perkalian = "+ op.Perkalian());
        
        System.out.println("Hasil Pembagian = "+ op.Pembagian());
    }
}
