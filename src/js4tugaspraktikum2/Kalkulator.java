/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package js4tugaspraktikum2;

/**
 *
 * @author ROG SERIE
 */
public class Kalkulator implements Operasi{
    public double Bil1,Bil2;
    
    public Kalkulator()
    {
        
    }
    public Kalkulator (double Bil1, double Bil2)
    {
        this.Bil1 = Bil1;
        this.Bil2 = Bil2;
    }
    public double getBil1(){
        return Bil1;
    } 
    public double getBil2(){
        return Bil2;
    }
    @Override
    public double Penjumlahan()
    {
        double  penj = 0;
        penj = this.Bil1 + this.Bil2;
        return penj;
    }
    
    @Override
    public double Pengurangan()
    {
        double  peng = 0;
        peng = this.Bil1 - this.Bil2;
        return peng;
    }
    
    @Override
    public double Perkalian()
    {
        double  per = 0;
        per = this.Bil1 * this.Bil2;
        return per;
    }
    
    @Override
    public double Pembagian()
    {
        double  pem = 0;
        pem  = this.Bil1 / this.Bil2;
        return pem;
    }
}
